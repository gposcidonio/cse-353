import matplotlib.pyplot as plt
import numpy as np

#Get the raw data array from the csv file
#raw_array = np.genfromtxt('training_data.csv',dtype=int,)
raw_array = np.genfromtxt('training_data.csv', dtype=int, delimiter=',')
print "Raw Array:\n"
print raw_array
print "\n\n\n"
#extract the columns that we want
training_data = raw_array[:, [1, 2, 3, 4, 5, 6]]
print "Training Data:\n"
print training_data
print "\n\n\n"
#extract overassessed data
overassessed_data = training_data[training_data[:, 5] == 1]
print"Overassessed Data:\n"
print overassessed_data
print"\n\n\n"
#extract gunderassessed_data
underassessed_data = training_data[training_data[:, 5] == -1]
print"Underassessed Data:\n"
print underassessed_data 
print"\n\n\n"
#
#extract good data
good_data = training_data[training_data[:, 5] == 0]
print"Good Data:\n"
print good_data
print"\n\n\n"


#first plot will show the square footage v. the assessment
plt.plot(good_data[:,3],good_data[:,1],'bo',label="Accurate")
plt.plot(underassessed_data[:,3],underassessed_data[:,1],'rs')
plt.plot(overassessed_data[:,3],overassessed_data[:,1],'g^')
plt.ylabel('Square Footage')
plt.xlabel('Assessment')
plt.axis([122000,1000000,1000,60000])
plt.show()

#now plot units v assessment
plt.plot(good_data[:,3],good_data[:,0],'bo',label="Accurate")
plt.plot(underassessed_data[:,3],underassessed_data[:,0],'rs')
plt.plot(overassessed_data[:,3],overassessed_data[:,0],'g^')
plt.ylabel('Units')
plt.xlabel('Assessment')
plt.axis([122000,1000000,0,100])
plt.show()

#now plot year v assessment
plt.plot(good_data[:,3],good_data[:,2],'bo',label="Accurate")
plt.plot(underassessed_data[:,3],underassessed_data[:,2],'rs')
plt.plot(overassessed_data[:,3],overassessed_data[:,2],'g^')
plt.ylabel('Year')
plt.xlabel('Assessment')
plt.axis([122000,1000000,1890,1960])
plt.show()

#trying square footage v indicated market value
plt.plot(good_data[:,4],good_data[:,1],'bo',label="Accurate")
plt.plot(underassessed_data[:,4],underassessed_data[:,1],'rs')
plt.plot(overassessed_data[:,4],overassessed_data[:,1],'g^')
plt.ylabel('Square Footage')
plt.xlabel('Indicated market Value')
plt.axis([500000,4000000,1000,60000])
plt.show()

