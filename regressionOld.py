import numpy as np
from numpy.linalg import inv
from sklearn.cross_validation import StratifiedKFold


# load the data from the csv file
#<<<<<<< HEAD
in_data = open("./training_data.csv", "r")  # the input data from the csv
raw_data = np.loadtext(in_data, delimeter=",") 

print raw_data

# need to declare arrays as special numpy arrays to invert them
data = raw_data[:, 0:6]  # training data matrix,   row, column     : every row,    then column 0 to 6
y = raw_data[:, 7]  # labels
#=======
in_data = open("./training_data.csv")  # the input data from the csv
in_data.readline()
raw_data = np.loadtxt(in_data, delimiter=",")

# need to declare arrays as special numpy arrays to invert them

########## approach 1
#data = raw_data[:, 0:4]  # training data
#print data

#y = raw_data[:, 6]  # 6th column being the labels

#transpose_data = data.transpose()  # transpose the matrix

# w = (X * X^T)^-1 * X^T * y
#weights = np.dot(np.dot(inv(np.dot(data,transpose_data)),transpose_data),y) # calculate the weights

#shuffle all rows of the matrix
shuffledRawDataMatrix = numpy.random.shuffle(raw_data[:]) 


#get number of rows in the matrix for percentage
numRows = len(shuffledRawDataMatrix)
partitionedShuffledRawDataMatrix = shuffledRawDataMatrix(numRows)

kfold = 5
rowsPerPart = kfold/numRows   #should be 20 rows in each part

for kthfold in range(1,kfold):
	testset = shuffledRawDataMatrix[kthfold:(kthfold+rowsPerPart-1), :];   #first time would take row1~row20

########## 

################################## approach 2
#shuffle all rows of the matrix
shuffledRawDataMatrix = numpy.random.shuffle(raw_data[:]) 

#get number of rows in the matrix for percentage
numRows = len(shuffledRawDataMatrix)
partitionedShuffledRawDataMatrix = shuffledRawDataMatrix(numRows)

numCols = len(shuffledRawDataMatrix[0])

	for i in xrange(kfold):  #should partition nth parts as the validation set
		trainingSets = [x for i, x in enumerate(X) if i % K != k]
		validation = [x for i, x in enumerate(X) if i % K == k]
        #yield training, validation
        trainingXs = np.array(training[:, 0:numCols-1])
        trainingYs = np.array(training[:, numCols])
        # w = (X * X^T)^-1 * X^T * y
        weights = np.dot(np.dot(inv(np.dot(trainingXs,trainingXs.transpose())),trainingXs.transpose()),trainingYs)
        wSum += weights
        predictedYs = np.dot(trainingXs, weights)  #unsure.......
        #validationX = np.array(validation[:, 0:numCols-1])
        validationY = np.array(validation[:, numCols])
        #validationWeight = np.dot(np.dot(inv(np.dot(validationX,validatinX.transpose())),validationX.transpose()),validationY)
        errorSum+= (predictedYs-validationY)
        #after for loop & adding all the different weights &summing errors

avgWeight = weights/kfold
avgError = errorSum/kfold

############################end of approach 2

#find w, avg all w's, then 
#accuracy score .py

#divide into 5 parts

#train 

#StratifiedKFold is a variation of K-fold, 
#which returns stratified folds, 
#i.e which creates folds by preserving the same percentage for each target class as in the complete set.
#instead of Y, input X matrix instead

#skf = StratifiedKFold(raw_data, 2)
#print skf
#sklearn.cross_validation.StratifiedKFold(labels=[0 0 0 1 1 1 0], k=2)

#for train, test in skf:
 #    print train, test
#[1 4 6] [0 2 3 5]
#[0 2 3 5] [1 4 6]