# This is file runs the rbf kernel
# Then it does 6-fold cross validation to see which one if the best

from sklearn import svm
from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt

#load the training data from the csv and skip the first row (headers)
raw_data = np.loadtxt("../data/training_data_norm.csv", delimiter=',', skiprows=1)
np.random.shuffle(raw_data)

#training set it the columns 0 to 7
training_data = raw_data[:, 0:7]


training_labels = raw_data[:, 7:]
#start by evaluating the rbf kernel
rbf_clf = svm.SVC(kernel="rbf")

#The error for each iteration of the kernel
cv_errors = []

#the total errors for the rbf kernel
rbf_errors = []

# the array of gamma values to test
gammas = [ .000001, .00001, .0001, .01, 1, 10]

#compute the number of rows in the test data set
#start with 15% of the data
#test_data_length = round(training_data.shape[0] - (training_data.shape[0]*.15))
#print "test data length: " + str(test_data_length)

test_data_length = 1000  # make the test data 109 examples

#get the test set from the last fold of the data
test_data = training_data[test_data_length:, :]
test_labels = training_labels[test_data_length:, :]

# redo the training data
training_data = training_data[0:test_data_length, :]
training_labels = training_labels[0:test_data_length, :]

# ndarray.shape returns a tuple: (number of rows, number of columns) if it's a 2d array
# or (number of cols) if it's a 1d array
# make the partition amount 1/6 the number of rows in the array
partition_amount = (training_data.shape[0])/6


# do 6 fold cross validation
for i in range(0, 6):
    print "gamma: " + str(gammas[i])
    # make a new classifier to test the cross validation

    #initializre the
    cv_errors = []

    for x in range(0, 6):
        rbf_clf = svm.SVC(kernel="rbf", gamma=gammas[i])

        #partition the training data and labels
        kfold_training_data = training_data[(x*partition_amount):((x+1)*partition_amount), :]
        kfold_training_labels = training_labels[(x*partition_amount):((x+1)*partition_amount), :]


        #reshape the labels array so it works correctly
        kfold_training_labels = kfold_training_labels.reshape(kfold_training_labels.shape[0])

        #train the classifier
        rbf_clf.fit(kfold_training_data, kfold_training_labels)

        #now classify new data in the test set
        predictions = rbf_clf.predict(test_data)

        #calculate the accuracy score (misclassification error) and add it to the array of scores
        cv_errors.append(accuracy_score(test_labels, predictions))

    #calculate the average misclassification error for the gamma value
    avg = 0
    for num in cv_errors:
        avg += num

    avg /= 6

    #add it to the array of values
    #accuracy score is the classification error, do 1-accuracy score to find misclassification error
    rbf_errors.append((1-avg))


#now plot the misclassification error for each gamma value
plt.ylabel("Misclassification Error")
plt.xlabel("Gamma Value")
plt.plot(gammas, rbf_errors)
plt.title("Misclassification Error vs. Gamma Value for the Cross Validated SVM")
plt.show()
