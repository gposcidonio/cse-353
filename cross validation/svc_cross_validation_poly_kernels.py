# This is file runs three different kernels: quadratic, linear, and rbf
# Then it does 6-fold cross validation to see which one if the best

from sklearn import svm
from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

#load the training data from the csv and skip the first row (headers)
raw_data = np.loadtxt("../data/training_data_norm.csv", delimiter=',', skiprows=1)
np.random.shuffle(raw_data)

#training set it the columns 0 to 7
training_data = raw_data[:, 0:7]

training_labels = raw_data[:, 7:]

#create a default linear kernel support vector machine
#and a polynomial support vector classidier with a degree of 4 (quadratic)
linear_clf = svm.LinearSVC()
quad_clf = svm.SVC(kernel="poly", degree=4)

#The error for each iteration of the kernel
linear_cv_errors = []
quad_cv_errors = []

#the total errors for the  kernels
linear_errors = []
quad_errors = []


#compute the number of rows in the test data set
#start with 15% of the data
#test_data_length = round(training_data.shape[0] - (training_data.shape[0]*.15))
#print "test data length: " + str(test_data_length)

test_data_length = 1000  # make the test data 109 examples

#get the test set from the last fold of the data
test_data = training_data[test_data_length:, :]
test_labels = training_labels[test_data_length:, :]

# redo the training data
training_data = training_data[0:test_data_length, :]
training_labels = training_labels[0:test_data_length, :]

# ndarray.shape returns a tuple: (number of rows, number of columns) if it's a 2d array
# or (number of cols) if it's a 1d array
# make the partition amount 1/6 the number of rows in the array
partition_amount = (training_data.shape[0])/6

# the array of number of items in the dataset
m = [ partition_amount, partition_amount*2, partition_amount*3, partition_amount*4, partition_amount*5, partition_amount*6]

# do 6 fold cross validation
for i in range(0, 6):
    print "m: " + str(m[i])
    # make a new classifier to test the cross validation

    #initialize the arrays of cross validation values
    linear_cv_errors = []
    quad_cv_errors = []

    for x in range(0, 6):
        linear_clf = svm.LinearSVC()
        quad_clf = svm.SVC(kernel="poly", degree=4)

        #partition the training data and labels
        kfold_training_data = training_data[0:m[i], :]
        kfold_training_labels = training_labels[0:m[i], :]


        #reshape the labels array so it works correctly
        kfold_training_labels = kfold_training_labels.reshape(kfold_training_labels.shape[0])

        #train the classifier
        linear_clf.fit(kfold_training_data, kfold_training_labels)
        quad_clf.fit(kfold_training_data, kfold_training_labels)


        #now classify new data in the test set
        linear_predictions = linear_clf.predict(test_data)
        quad_predictions = quad_clf.predict(test_data)

        #calculate the accuracy score (misclassification error) and add it to the array of scores
        linear_cv_errors.append(accuracy_score(test_labels, linear_predictions))
        quad_cv_errors.append(accuracy_score(test_labels, quad_predictions))

    #calculate the average misclassification error for the m value
    avg = 0
    for num in linear_cv_errors:
        avg += num

    avg /= 6

    #add it to the array of values
    #accuracy score is the classification error, do 1-accuracy score to find misclassification error
    linear_errors.append((1-avg))

    avg = 0
    for num in quad_cv_errors:
        avg += num

    avg /= 6

    #add it to the array of values
    #accuracy score is the classification error, do 1-accuracy score to find misclassification error
    quad_errors.append((1-avg))


#now plot the misclassification error for each gamma value
plt.ylabel("Misclassification Error")
plt.xlabel("m Values (Number of Samples)")
p1 = plt.plot(m, linear_errors)
p2 = plt.plot(m, quad_errors)
plt.setp(p1, color='red', linewidth=2.0)
plt.setp(p2, color='blue', linewidth=2.0)
plt.title("Misclassification Error vs. m Value for Polynomial SVM's")

#create a legend for the plot
red_patch = mpatches.Patch(color='red', label='Linear SVC')
blue_patch = mpatches.Patch(color='blue', label='Quadratic SVC')
plt.legend(handles=[red_patch, blue_patch])

plt.show()
