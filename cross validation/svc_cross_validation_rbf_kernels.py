# This is file runs the rbf kernel
# Then it does 6-fold cross validation to see which one if the best
# graphs the learning curve (m values) for each gamma value

from sklearn import svm
from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt

#load the training data from the csv and skip the first row (headers)
raw_data = np.loadtxt("../data/training_data_norm.csv", delimiter=',', skiprows=1)
np.random.shuffle(raw_data)

#training set it the columns 0 to 7
training_data = raw_data[:, 0:6]

training_labels = raw_data[:, 6:]

# the array of gamma values to test
gammas = [.000001, .00001, .0001, .01, 1, 10]

#Initaliaze all the rbf SVM classifiers and put them in an array
clfs = [svm.SVC(kernel="rbf", gamma=gammas[i]) for i in range(0, 6)]

#the total errors for the rbf kernel
#each row if a classifier, each column is an m value
rbf_errors = np.zeros([6, 6])


#compute the number of rows in the test data set
#start with 15% of the data
test_data_length = round(training_data.shape[0] - (training_data.shape[0]*.15))
#print "test data length: " + str(test_data_length)

#test_data_length = 1000  # make the test data 109 examples
print "raw data shpe"
print raw_data.shape

#get the test set from the last fold of the data
test_data = training_data[test_data_length:, :]
test_labels = training_labels[test_data_length:, :]

# redo the training data
training_data = training_data[0:test_data_length, :]
training_labels = training_labels[0:test_data_length, :]

# ndarray.shape returns a tuple: (number of rows, number of columns) if it's a 2d array
# or (number of cols) if it's a 1d array
# make the partition amount 1/6 the number of rows in the array
partition_amount = (training_data.shape[0])/6

# the array of number of items in the dataset
m = [partition_amount, partition_amount*2, partition_amount*3, partition_amount*4, partition_amount*5, partition_amount*6]

# loop over all the m values
for x in range(0, 6):
    #partition the training data and labels
    kfold_training_data = training_data[0:m[x], :]
    kfold_training_labels = training_labels[0:m[x]]

    print "training data shape:"
    print kfold_training_data.shape

    #reshape the labels array so it works correctly
    np.reshape(kfold_training_labels, kfold_training_labels.shape[0])

    #flatten the labels
    kfold_training_labels = np.ravel(kfold_training_labels)

    print "training_labels shape"
    print kfold_training_labels

    #train each classifier
    #predict the data, find the misclassification error and add it to the array
    for j in range(0, 6):
        clfs[j].fit(kfold_training_data, kfold_training_labels)
        print test_data

        #now classify new data in the test set
        predictions = clfs[j].predict(test_data)

        rbf_errors[j][x] = 1 - accuracy_score(test_labels, predictions)


#now plot the misclassification error for each gamma value
plt.ylabel("Misclassification Error")
plt.xlabel("m Value")
plts = [plt.plot(m, rbf_errors[i], label="Gamma value" + str(gammas[i])) for i in range(0, 6)]  # create the arrays of plots
plt.legend(loc = 'upper left')
plt.title("Misclassification error vs. m value for Gaussian Kernel SVM's")
plt.show()
