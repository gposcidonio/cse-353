import numpy as np
import scikitlearn.SVM as SVC

clf = SVC(kernel = "rbf")  # radial basis function kernel support vector classifier

training_data = [];  # the array of training data
test_data = []  # the array of data we are trying to classify
labels = [-1, 0, 1] # the array of labels, -1(underassesd) , 0 (good) , 1 (overassed)

clf.fit(training_data,labels)  # fit the SVC to the data so it can predict new examples

test_labels = clf.predict(test_data)

