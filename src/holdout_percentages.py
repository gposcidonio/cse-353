#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures
from numpy import sign

degrees = [1, 2, 3, 5, 10]
colors = ['r', 'g', 'b', 'm', 'y', 'c']
holdout_percentages = [0.85, 0.60, 0.40, 0.25, 0.15]
lasso = linear_model.Lasso(alpha=0.1)


def lasso_classifier(file_name):

    # Load the data from the csv file and count the number of records.
    raw_data = np.loadtxt(file_name, delimiter=',', skiprows=1)
    np.random.shuffle(raw_data)
    n = len(raw_data)

    errors = np.zeros((len(holdout_percentages), len(degrees)))
    for d in xrange(len(degrees)):

        poly = PolynomialFeatures(degree=degrees[d], include_bias=False)

        for h in xrange(len(holdout_percentages)):

            holdout_size = round((holdout_percentages[h] * n) / 5.0) * 5.0
            train = raw_data[:-holdout_size, :]
            test = raw_data[-holdout_size:, :]
            train_x = poly.fit_transform(train[:, :-1])
            train_y = train[:, -1]
            test_x = poly.fit_transform(test[:, :-1])
            test_y = test[:, -1]

            # Transform the data.
            coeff = lasso.fit(train_x, train_y)
            results = lasso.predict(test_x)
            total_errors = sum(abs(test_y - sign(results)) / 2.0)
            errors[h, d] = total_errors / len(test_y)

    plot_errors(errors, "Lasso Classifier")


def plot_errors(errors, title):

    """ Plot the test error as a function of the number of training examples and save to './figures'. """

    # Plot the errors to the graph.
    for d in xrange(len(degrees)):
        plt.plot(holdout_percentages, errors[:, d], color=colors[d], label=degrees[d])
    # Show the graph.
    plt.title(title)
    plt.legend()
    plt.ylabel('Error Percentage')
    plt.xlabel('Holdout Size (as percentage)')
    plt.savefig("./figures/" + title + ".png")
    plt.show()


def print_help():

    """ Print the usage of lasso_classifier.py to the console. """

    print "Usage: "


#lasso_classifier('../data/training_data_norm.csv')


# Run from command line.
if __name__ == '__main__':
    if len(sys.argv) < 3:
        print_help()
    else:
        data_path = sys.argv[1]
        selection = sys.argv[2]
        if selection == "help":
            print_help()
        elif selection == "lasso":
            lasso_classifier(data_path)
        else:
            print_help()
