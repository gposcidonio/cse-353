#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from random import randint
from random import shuffle
from numpy.linalg import inv
from numpy import dot
from numpy import transpose
from numpy import sign
from sklearn.cross_validation import StratifiedKFold


def transform_data(data):

    num_features = len(data[0]) - 1
    num_records = len(data)
    n = (num_features * (num_features + 1)) / 2.0

    phi = np.zeros((num_records, n + 1))

    # Every pair of features,
    for k in xrange(num_records):
        m = 0
        for i in xrange(num_features):
            for j in xrange(i, num_features):
                phi[k, m] = data[k, i] * data[k, j]
                m += 1

        phi[k, m] = data[k, num_features]

    return phi


def general_regression(file_name, k):

    """Performs k-fold cross-validated general regression on the data matrix
       specified by the csv document with file_name."""

    # Load the data from the csv file and shuffle the points.
    raw_data = np.loadtxt(file_name, delimiter=',', skiprows=1)
    # shuffle(raw_data)

    phi = transform_data(raw_data)

    # Get number of features from raw data.
    num_features = len(phi[0]) - 1

    # Partition the data into k segments.
    partitions = np.array_split(phi, k)

    # Initialize the weights vector.
    weights = np.zeros((k, num_features))
    errors = np.zeros(k)

    # Initialize the tracking information.
    best_weights_index = 0
    lowest_error = float("inf")
    total_avg_error = 0

    # Apply k-fold cross-validation.
    for test_index in xrange(k):

        # Perform Linear Regression on each of the partitions.
        for i in xrange(k):

            # Skip the text index.
            if i == test_index:
                continue

            # Grab all features except for last column.
            x = partitions[i][:, : num_features]

            # Grab the classes (last column).
            y = partitions[i][:, -1]

            # Solve linear expression and update weights.
            x_t = transpose(x)
            pseudo_inverse = inv(dot(x_t, x))
            weights[test_index] += dot(pseudo_inverse, dot(x_t, y))

        # Generate average weights for this cross-validation.
        weights[test_index] /= k

        # Test the weights on the test partition. #
        test_x = partitions[test_index][:, :num_features]
        test_y = partitions[test_index][:, -1]
        output = dot(test_x, weights[test_index])

        # Calculate the error.
        error = 0
        for j in xrange(len(output)):
            classification = sign(output[i])
            error += abs(test_y[j] - classification)

        # Update best record.
        errors[test_index] = error / (len(output) * 2)
        if errors[test_index] < lowest_error:
            lowest_error = errors[test_index]
            best_weights_index = test_index

    # Determine the weights with the lowest error.
    print('Lowest error: ')
    print(lowest_error)
    print('Lowest error weights: ')
    print(weights[best_weights_index])
    print('Total errors: ')
    for i in xrange(k):
        print(errors[i])

    return weights[best_weights_index]

# Run from command line.
if __name__ == '__main__':
    general_regression('./training_data_norm.csv', 6)