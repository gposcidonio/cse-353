#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from random import randint
from random import shuffle
from numpy.linalg import inv
from numpy import dot
from numpy import transpose
from numpy import sign
from sklearn.cross_validation import StratifiedKFold


def linear_regression(file_name, k):

    """Performs k-fold cross-validated linear regression on the data matrix
       specified by the csv document with file_name."""

    # Load the data from the csv file and shuffle the points.
    raw_data = np.loadtxt(file_name, delimiter=',', skiprows=1)

    # Get number of features from raw data.
    num_features = len(raw_data[0]) - 1

    data_size = [40, 80, 180, 360, 650]

    for s in data_size:

        filtered_data = raw_data[:s]

        # Partition the data into k segments.
        partitions = np.array_split(filtered_data, k)

        # Initialize the weights vector.
        weights = np.zeros((k, num_features))

        # Initialize the tracking information.
        best_weights_index = 0
        lowest_error = float("inf")
        total_avg_error = 0

        # Apply k-fold cross-validation.
        for test_index in xrange(k):

            # Perform Linear Regression on each of the partitions.
            for i in xrange(k):

                # Skip the text index.
                if i == test_index:
                    continue

                # Grab all features except for last column.
                x = partitions[i][:, : num_features]

                # Grab the classes (last column).
                y = partitions[i][:, -1]

                # Solve linear expression and update weights.
                x_t = transpose(x)
                pseudo_inverse = inv(dot(x_t, x))
                weights[test_index] += dot(pseudo_inverse, dot(x_t, y))

            # Generate average weights for this cross-validation.
            weights[test_index] /= k

            # Test the weights on the test partition. #
            test_x = partitions[test_index][:, :num_features]
            test_y = partitions[test_index][:, -1]
            output = dot(test_x, weights[test_index])

            # Calculate the error.
            error = 0
            for j in xrange(len(output)):
                classification = sign(output[i])
                error += abs(test_y[j] - classification)

            # Update best record.
            avg_error = error / (len(output) * 2)
            if avg_error < lowest_error:
                lowest_error = avg_error
                best_weights_index = test_index

        # Determine the weights with the lowest error.
        print('Size ')
        print(s)
        print(' lowest error: ')
        print(lowest_error)

    # return weights[best_weights_index]

# Run from command line.
if __name__ == '__main__':
    linear_regression('./training_data_norm.csv', 5)